jQuery(document).ready(function ($) {

	var db = openDatabase("cadastro", "1.0", "cadastro", 100000);

	function aviso_erro(transaction, erro){
	  alert("Erro no banco: " + erro.message);
	  return false;
	};

	$("#criar_tabela").bind("click", function(event){
	  db.transaction(function(transaction){ 
		var sql = "CREATE TABLE pessoas " +
				  "(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +	
				  "nome VARCHAR(100) NOT NULL, " +
				  "sobrenome VARCHAR(100) NOT NULL)";
		transaction.executeSql(sql,undefined, function() {
		  alert("Tabela cridada!");
		}, aviso_erro);  
	  });	
	});

	$("#excluir_tabela").bind("click", function(event){
	  if (!confirm("Apagar tabela?", "")) return;
	  db.transaction(function(transaction){ 
		var sql = "DROP TABLE pessoas";
		transaction.executeSql(sql,undefined, function(){}, aviso_erro);
	  });
	});

	$("#inserir_pessoa").bind("click", function(event){
	  var nome = $("#nome").val();
	  var sobrenome = $("#sobrenome").val();  
	  db.transaction(function(transaction){ 
		var sql = "INSERT INTO pessoas (nome, sobrenome) VALUES(?,?)";
		transaction.executeSql(sql,[nome,sobrenome], function(){
		  carregarListagem();
		}, aviso_erro);
	  });
	});

	function carregarListagem(){
	  db.transaction(function(transaction){   
		var sql = "SELECT * FROM pessoas";
		transaction.executeSql(sql,undefined, 
		  function(transaction, resultado){
			
			var html = "<ul>";
			if(resultado.rows.length) {
			  for(var i = 0; i < resultado.rows.length; i++) {
				var linha = resultado.rows.item(i);
				var nome = linha.nome;
				var sobrenome = linha.sobrenome;
				var id = linha.id;
				html += "<li data-icon=false id='" + id + "'>";		  
				html +=    "<a href='#'>";		  
				html +=       nome + "&nbsp;" + sobrenome;		  
				html +=    "</a>";		  
				html += "</li>";		  
			  }
			} else {
			  html += "<li> Nenhuma pessoa cadastrada.</li>";
			}
			html += "</ul>";
				
			$("#paginaDois").unbind().bind("pagebeforeshow", function(){
			  var $content = $("#paginaDois div:jqmData(role=content)");
			  $content.html(html);
			  var $ul = $content.find("ul");
			  $ul.listview();
			   
				$("li").bind("taphold",function(){
					 var id = $(this).attr("id");			 
					 if(!id) return;
					 if (!confirm("Confirma exclusão?", "")) return;	
					 $(this).remove();
					 db.transaction(function(transaction){
						 var sql = "DELETE FROM pessoas WHERE id=?";
						 transaction.executeSql(sql,[id],undefined, aviso_erro);
					 });
        });   
			});
			
			$.mobile.changePage($("#paginaDois"));
			
		  }, aviso_erro);
	  });
	};
	
	$("#listar_pessoa").bind("click", function(event){
	  carregarListagem();
	});
});